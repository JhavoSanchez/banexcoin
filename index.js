const express = require('express');

const app = express();

// midddlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// routes
app.use(require('./src/app/routes/app.routes'));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
