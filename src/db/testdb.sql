CREATE DATABASE testdb;


DROP TABLE IF EXISTS clientes;
CREATE TABLE clientes (
    id SERIAL PRIMARY KEY,
    created TIMESTAMP NULL,
    updated TIMESTAMP NULL,
    status BOOLEAN DEFAULT '1',
    fname VARCHAR(50) NULL,
    lname VARCHAR(50) NULL,
    address TEXT NULL,
    birthdate DATE NULL
);


DROP PROCEDURE IF EXISTS sp_list_customer();
CREATE FUNCTION f_list_customer(
	pId INT DEFAULT 0
) 
RETURNS SETOF clientes 
AS 
$$ 
BEGIN 
	IF pId = 0 THEN
		RETURN QUERY SELECT * FROM clientes;
	ELSE
		RETURN QUERY SELECT * FROM clientes WHERE id = pId;
	END IF;
END
$$ 
LANGUAGE plpgsql;


DROP PROCEDURE IF EXISTS sp_create_customer();
CREATE PROCEDURE sp_create_customer(
	pFname VARCHAR(50),
	pLname VARCHAR(50),
	pAddress TEXT,
	pBirthdate DATE
) 
LANGUAGE plpgsql
AS $$
BEGIN
	INSERT INTO clientes (created, fname, lname, address, birthdate) 
	VALUES (CURRENT_TIMESTAMP, pFname, pLname, pAddress, pBirthdate);

	COMMIT;
END;
$$

CALL sp_create_customer('Harvey', 'Sanchez', 'SMP', '1992-07-29');
CALL sp_create_customer('Nahuel', 'Sanchez', 'San Luis', '2014-11-15');


DROP PROCEDURE IF EXISTS sp_update_customer();
CREATE PROCEDURE sp_update_customer(
	pId INT,
	pStatus BOOLEAN,
	pFname VARCHAR(50),
	pLname VARCHAR(50),
	pAddress TEXT,
	pBirthdate DATE
) 
LANGUAGE plpgsql
AS $$
BEGIN
	UPDATE clientes 
	SET 
		updated = CURRENT_TIMESTAMP,
		status = pStatus,
		fname = pFname,
		lname = pLname,
		address = pAddress,
		birthdate = pBirthdate
	WHERE id = pId;

	COMMIT;
END;
$$