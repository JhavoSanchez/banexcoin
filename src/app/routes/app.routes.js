const { Router } = require('express');
const router = Router();

const { createCustomer, getCustomers, getCustomerById, updateCustomer } = require('../controllers/app.controller');

router.post('/api/v1/clientes/registro', createCustomer);
router.get('/api/v1/clientes', getCustomers);
router.get('/api/v1/clientes/:id', getCustomerById);
router.post('/api/v1/clientes/:id', updateCustomer);

module.exports = router;