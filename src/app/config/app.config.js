module.exports = {
    HOST: 'localhost',
    USER: 'postgres',
    PASSWORD: 'root',
    DB: 'testdb',
    PORT: '5432'
}