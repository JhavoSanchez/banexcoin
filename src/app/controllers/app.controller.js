const appConfig = require('../config/app.config');
const { Pool } = require('pg');

const pool = new Pool({
    host: appConfig.HOST,
    user: appConfig.USER,
    password: appConfig.PASSWORD,
    database: appConfig.DB,
    port: appConfig.PORT
});

const createCustomer = async (req, res) => {
    const { fname, lname, address, birthdate } = req.body;
    const response = await pool.query('CALL sp_create_customer($1, $2, $3, $4)', [ fname, lname, address, birthdate ]);
    console.log(response);
    res.json({
        message: 'Cliente registrado satisfactoriamente.',
        body: {
            customer: { fname, lname, address, birthdate }
        }
    })
}

const getCustomers = async (req, res) => {
    const response = await pool.query('SELECT f_list_customer()');
    res.status(200).json(response.rows);
}

const getCustomerById = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT f_list_customer($1)', [ id ]);
    res.status(200).json(response.rows);
}

const updateCustomer = async (req, res) => {
    const { id, status, fname, lname, address, birthdate } = req.body;
    const response = await pool.query('CALL sp_update_customer($1, $2, $3, $4, $5, $6)', [ id, status, fname, lname, address, birthdate ]);
    console.log(response);
    res.json({
        message: 'Cliente actualizado satisfactoriamente.',
        body: {
            customer: { id, status, fname, lname, address, birthdate }
        }
    })
}

module.exports = {
    createCustomer,
    getCustomers,
    getCustomerById,
    updateCustomer
}